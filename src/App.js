import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Footer from "./components/Footer"
import Navbar from "./components/Navbar"

import Admin from "./pages/Admin";
import User from "./pages/User"
import Home from "./pages/Home"
import AllProduct from "./pages/AllProduct"
import Buy from './pages/Buy';
import DashBoard from './pages/DashBoard';
import CheckOut from "./pages/CheckOut";

function App() {
  return (
    <Router>
      <Navbar />
        <Routes>
          <Route path='/' element= {<Home />} />
          <Route path='/user' element = {<User />} />
          <Route path='/AllProduct' element = {<AllProduct />} />
          <Route path='/buy/:id' element = {<Buy />} />
          <Route path='/dashboard/:id' element = {<DashBoard />} />
          <Route path='/admin' element = {<Admin />} />
          <Route path='/checkout/:id' element = {<CheckOut />} />
        </Routes>
      <Footer />
    </Router>
  )
}

export default App;
