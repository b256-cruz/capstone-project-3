import Spacer from "../components/Spacer"
import UserDashboard from "../components/UserDashboard"


export default function Dashboard() {
    return(
        <>
            <Spacer />
            <UserDashboard />
        </>
    )
}