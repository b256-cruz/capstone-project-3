import AdminDashboard from "../components/AdminDashboard"
import Spacer from "../components/Spacer"


export default function Admin () {
    return (
        <>
            <Spacer />
            <AdminDashboard />
        </>
    )
}