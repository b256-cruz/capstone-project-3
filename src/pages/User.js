import LoginAndRegisterComponent from "../components/LoginAndRegisterComponent"
import Spacer from "../components/Spacer"



export default function LoginAndRegister () {
    return (
        <>
            <Spacer />
            <LoginAndRegisterComponent />
        </>
    )
}