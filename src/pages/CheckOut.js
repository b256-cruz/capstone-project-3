import Spacer from "../components/Spacer"
import CheckoutComponents from "../components/CheckoutComponents"

export default function CheckOut() {
    return (
        <>
            <Spacer />
            <CheckoutComponents />
        </>
    )
}