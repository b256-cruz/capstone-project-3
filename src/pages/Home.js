import Carsousel from "../components/Carousel"
import Community from "../components/Community"
import ProductTechnology from "../components/ProductTechnology"


export default function Home() {
    return (
        <>
            <Carsousel />
            <Community />
            <ProductTechnology />
        </>
    )
}