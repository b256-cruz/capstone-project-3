import ProductInfoComponent from "../components/ProductInfoComponent"
import Spacer from "../components/Spacer"

export default function Buy() {
    return (
        <>
            <Spacer />
            <ProductInfoComponent />
        </>

    )
}