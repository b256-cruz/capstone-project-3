import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"


export default function CheckoutComponents () {

    const { id } = useParams()
    const [address, setAddress] = useState('');
    const [user, setUser] = useState([])
    const [cart, setCart] = useState([])

    useEffect(() => {
        async function fetchData(){
            try {
                const userResponse = await fetch(`http://localhost:4000/users/retrieveUser/${id}`,{
                    method: "GET",
                    credentials: "include"
                })
                const userData = await userResponse.json();
                setUser(userData)
            } catch (err) {
                console.log(err);
            }
        } 
        fetchData()

        async function fetchCart() {
            try {
                const cartResponse = await fetch(`http://localhost:4000/users/cart/${id}`,{
                    method: "GET",
                    credentials: "include"
                })
                const cartData = await cartResponse.json()
                setCart(cartData)
                console.log(cartData)
            } catch (err) {
                console.log(err);
            }
        }
        fetchCart()
    },[])

    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    };


    const userCart = cart.userCart && cart.userCart.map((userCart) => {
        return(
            <>
                <div style={{ display: 'flex' }}>
                    <img src={userCart.orderPic} width="200px"></img>
                    <div>
                        <h1>{userCart.orderName}</h1>
                        <p>{userCart.orderPrice}</p>
                        <p>{userCart.orderQuantity}</p>
                        <p>{userCart.orderSize}</p>
                    </div>
                </div>
            </>
        )
    })

    const orderCartUserCart = cart.userCart && cart.userCart.map((userCart) => {
        return userCart
    })


  const handleSubmit = async (e) => {
    try {
        e.preventDefault()
        const checkOutResponse = await fetch(`http://localhost:4000/orders/checkout/${id}`,{
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            credentials: "include",
            body: JSON.stringify({
                address: address,
                orderCart: orderCartUserCart,
                orderPrice: cart.totalPrice
            })
        })
        const checkOutData = await checkOutResponse.json()
        console.log(checkOutData)
        
    } catch (err) {
        console.log(err)
    }
  };


    return (
        <div>
            <h1>Checkout Page</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="address">Address:</label>
                    <input
                        type="text"
                        id="address"
                        value={address}
                        onChange={handleAddressChange}
                        required
                    />
                </div>
                <div>
                    <label htmlFor="name">Name:</label>
                <span> {user.firstName}</span>
                </div>
                <div>
                    <label htmlFor="lastName">Last Name:</label>
                <span> {user.lastName}</span>
                </div>
                <button type="submit">Submit</button>
            </form>
            <div>
                {userCart}
            </div>
        </div>
  );
};