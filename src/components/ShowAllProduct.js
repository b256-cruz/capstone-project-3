import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import "../scss/showAllProducts.scss"


export default function ShowAllProduct() {

    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function fetchData() {
            try {
                const bannerResponse = await fetch("http://localhost:4000/products/all")
                const bannerData = await bannerResponse.json()
                setData(bannerData)
                setLoading(false)
            } catch (err) {
                console.log(err)
            }
        }
        fetchData()
    },[])


    const productBanner = data && data.map((data)=>{
        return(
            <Link to={`/buy/${data._id}`}>
                <button>
                    <img src= {data.bannerPicUrl}></img>
                </button>
            </Link>
        )
    })
    


    return (
        <section className="showAllProduct_container">
            <h1>ALL PRODUCTS</h1>
            {loading ? (
                <p>Loading...</p>
            ) : (
                <>
                    {productBanner}
                </>
            )}
        </section>
    )
}