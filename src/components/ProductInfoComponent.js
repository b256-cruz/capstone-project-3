import { useEffect, useState } from "react";
import { useParams } from 'react-router-dom';
import "../scss/productInfoComponent.scss";

export default function ProductInfoComponent() {

    const { id } = useParams()
    const [loading, setLoading] = useState(true);
    const [product, setProduct] = useState(null);
    const [color, setColor] = useState(0)
    const [mainPic, setMainpic] = useState(0)


    const [orderId, setOrderId] = useState("")
    const [orderPic, setOrderPic] = useState("https://lh3.googleusercontent.com/pw/AJFCJaV4wXC1OQMs22cyVNtHCM74hC4y9SdTkeolZp-s3Ccoebv01vWVVdAJLNGkoy436NU8ouSe-pRHI8fPVLnzSEuNkE7rFMBr4csPRf-mEXPlraUbJxqMHm-49B9iDBqLceDI9noOAUWpkUJ4p1Ods5s=w943-h943-s-no?authuser=0")
    const [orderName, setOrderName] = useState("Vacuum-sealed Water Bottle Classic Colors")
    const [orderPrice, setOrderPrice] = useState(1000)
    const [orderQuantity, setOrderQuantity] = useState(1)
    const [orderSize, setOrderSize] = useState("750 ml")
    const [orderColor, setOrderColor] = useState("red")
    

    useEffect(() => {
        async function fetchData() {
            try {
                const productId = id;
                const productResponse = await fetch(`http://localhost:4000/products/singleItem/${productId}`);
                const productData = await productResponse.json();
                setProduct(productData);
                setOrderId(productId)
                setOrderName(productData.name)
                setLoading(false);
                console.log(productData)
            } catch (err) {
                console.log(err);
            }
        }
        fetchData();
    }, []);

    const productSizes = product && product.price.map((price) => {
        return (
            <button onClick={(e) => {
                e.preventDefault();
                setOrderPrice(price.amount);
                setOrderSize(price.size);
            }}>
                {price.size}
            </button>
        );
    });
    
    const colorOptions = product && product.option.map((option,index) => {
        return (
            <button onClick={(e) => {
                e.preventDefault();
                setColor(index)
                setOrderColor(option.color)
                setOrderPic(product.option[color].pictureUrl[0].link)
            }}
            style={{ backgroundColor: `${option.hex}` }}>

            </button>
        )
    })

    

    const addToCart = async(e) => {
        try {
            e.preventDefault()
            const cartResponse = await fetch("http://localhost:4000/users/addToCart", {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                },
                credentials: "include",
                body: JSON.stringify({
                    productId: orderId,
                    orderName: orderName,
                    orderPic: orderPic,
                    orderPrice: orderPrice,
                    orderQuantity: orderQuantity,
                    orderSize: orderSize,
                    orderColor: orderColor
                })
            })
            const cartData = await cartResponse.json()

            console.log(cartData)

            if(cartData === true) {
                alert("success")
            }else{
                alert("something went wrong")
            }
        } catch(err) {
            console.log(err)
        }

    }

    const changeQuantityMinus = ((e) => {
        e.preventDefault()
        if (orderQuantity > 0) {
            setOrderQuantity(orderQuantity - 1);
        }
    })

    const changeQuantityPlus = ((e) => {
        e.preventDefault()
        setOrderQuantity(orderQuantity + 1)
    })


    return (
        <section className="product_info_component_container">
            {loading ? (
                <p>Loading...</p>
            ) : (
                <>
                    <div className="product_info_component_leftSide_container">
                        <div>
                            <div>
                                <button onClick={(e) => {setMainpic(0)}}>
                                    <img src={product.option[color].pictureUrl[0].link}></img>
                                </button>
                                <button onClick={(e) => {setMainpic(1)}}>
                                    <img src={product.option[color].pictureUrl[1].link}></img>
                                </button>
                            </div>
                            <div>
                                <img src={product.option[color].pictureUrl[mainPic].link}></img>
                            </div>
                        </div>
                        <div>

                        </div>
                    </div>
                    <form onSubmit={addToCart} className="product_info_component_rightSide_container">
                        <h1>{product.name}</h1>
                        <p>&#8369;{orderPrice}</p>
                        <div>
                            <h2>Description and Specs</h2>
                            <p>{product.description}</p>
                        </div>
                        <div>
                            <h2>Quantities</h2>
                            <div>
                                <button onClick = {changeQuantityMinus}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24">
                                        <path fill="currentColor" d="M3 11h18v2H3z"/>
                                    </svg>
                                </button>
                                <p>{orderQuantity}</p>
                                <button onClick = {changeQuantityPlus}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24">
                                        <path fill="currentColor" d="M21 13h-8v8h-2v-8H3v-2h8V3h2v8h8v2z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div>
                            <h2>Sizes</h2>
                            <div>
                                {productSizes}
                            </div>
                        </div>
                        <div>
                            <h2>Colors</h2>
                            <div>
                                {colorOptions}
                            </div>
                        </div>
                        <button type="submit">ADD TO CART</button>
                    </form>
                </>
            )}
        </section>
    );
}
