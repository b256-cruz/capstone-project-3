import { useEffect, useState } from "react"


export default function AdminDashboard() {

    const [product, setData] = useState([])
    const [createProduct, setCreateProduct] = useState(false)

    useEffect(() => {
        async function fetchData() {
            try {
                const productResponse = await fetch("http://localhost:4000/products/all")
                const productData = await productResponse.json()
                setData(productData)
            } catch (err) {
                console.log(err)
            }
        }
        fetchData()
    },[])

    const activateProduct = (async (e) => {
        try {
            await fetch(`http://localhost:4000/products/${e}/activate`,{
                method: "PATCH",
                credentials: "include",
            })
            window.location.reload()
        } catch (err) {
            console.log(err)
        }
    })

    const archiveProduct = ( async (e) => {
        try {
            await fetch(`http://localhost:4000/products/${e}/archive`,{
                method: "PATCH",
                credentials: "include",
            })
            window.location.reload()
        } catch (err) {
            console.log(err)
        }
    })

    const tableRow = product.map((product)=> {
        const price = product.price.map((e) =>{
            return(
                <p>{e.size}: {e.amount}</p>
            )
        })
        return (
            <tr>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{price}</td>
                <td>
                    {
                        (product.isActive === true)?
                        <>
                            Available
                        </>
                        :
                        <>
                            Not Available
                        </>
                    }
                </td>
                <td>
                    <button>Edit</button>
                    <button onClick={() => {activateProduct(product._id)}}>Activate</button>
                    <button onClick={() => {archiveProduct(product._id)}}>Archive</button>
                </td>
            </tr>
        )
    })
    
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [productType, setProductType] = useState("");
    const [bannerPicUrl, setBannerPicUrl] = useState("");
    const [options, setOptions] = useState([
        { optionId: "", color: "", hex: "", pictureUrl: [""] },
    ]);
    const [prices, setPrices] = useState([{ priceId: "", size: "", amount: "" }]);

    const addOption = () => {
        setOptions([...options, { optionId: "", color: "", hex: "", pictureUrl: [""] }]);
    };

    const addPrice = () => {
        setPrices([...prices, { priceId: "", size: "", amount: "" }]);
    };

    const handlePictureUrlChange = (optionIndex, pictureIndex, event) => {
        const { value } = event.target;
        const updatedOptions = [...options];
        updatedOptions[optionIndex].pictureUrl[pictureIndex] = { link: value };
        setOptions(updatedOptions);
    };

    const handleOptionChange = (index, event) => {
        const { name, value } = event.target;
        const updatedOptions = [...options];
        updatedOptions[index][name] = value;
        setOptions(updatedOptions);
    };

    const handlePriceChange = (index, event) => {
        const { name, value } = event.target;
        const updatedPrices = [...prices];
        updatedPrices[index][name] = value;
        setPrices(updatedPrices);
    };

    const handleSubmit = async (e) => {

        const productinfo = {
            name: name,
            description: description,
            productType: productType,
            option: options,
            price: prices,
            bannerPicUrl: bannerPicUrl
        };

        try {
            e.preventDefault()
            const productResponse = await fetch("http://localhost:4000/products/create", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                credentials: "include",
                body: JSON.stringify(productinfo)
            })
        
            const productData = await productResponse.json()
            console.log(productData)
        } catch (err) {
            console.log(err);
        }
    };


    return (
        <>
            <section>
                <div>
                    <h1>Admin Dashboard</h1>
                    <button onClick={(e) => {
                        e.preventDefault()
                        setCreateProduct(true)
                    }}>
                        Add New Product
                    </button>
                    <button>
                        Show user Orders
                    </button>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Availability</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableRow}
                    </tbody>
                </table>
            </section>
            
            {
                (createProduct === false)?
                <>
                    <h1>Welcome</h1>
                </>
                :
                <>
                    <section>
                        <form onSubmit={handleSubmit}>

                        <fieldset>
                            <label htmlFor="name">Name:</label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                value={name}
                                onChange={(event) => setName(event.target.value)}
                                required
                            />

                            <label htmlFor="description">Description:</label>
                            <input
                                type="text"
                                id="description"
                                name="description"
                                value={description}
                                onChange={(event) => setDescription(event.target.value)}
                                required
                            />

                            <label htmlFor="productType">Product Type:</label>
                            <input
                                type="text"
                                id="productType"
                                name="productType"
                                value={productType}
                                onChange={(event) => setProductType(event.target.value)}
                                required
                            />
                        </fieldset>

                            <fieldset>
                            <legend>Options:</legend>
                            {options.map((option, index) => (
                                <div className="option" key={index}>
                                    <label htmlFor={`optionId${index + 1}`}>Option ID:</label>
                                    <input
                                        type="number"
                                        id={`optionId${index + 1}`}
                                        name="optionId"
                                        value={option.optionId}
                                        onChange={(event) => handleOptionChange(index, event)}
                                        required
                                    />

                                    <label htmlFor={`color${index + 1}`}>Color:</label>
                                    <input
                                        type="text"
                                        id={`color${index + 1}`}
                                        name="color"
                                        value={option.color}
                                        onChange={(event) => handleOptionChange(index, event)}
                                        required
                                    />

                                    <label htmlFor={`hex${index + 1}`}>Hex:</label>
                                    <input
                                        type="text"
                                        id={`hex${index + 1}`}
                                        name="hex"
                                        value={option.hex}
                                        onChange={(event) => handleOptionChange(index, event)}
                                        required
                                    />

                                    <label htmlFor={`pictureUrl${index + 1}`}>Picture URL:</label>
                                    {option.pictureUrl.map((picture, pictureIndex) => (
                                        <div key={pictureIndex}>
                                            <label htmlFor={`pictureUrl${index}-${pictureIndex}`}>
                                            Picture URL {pictureIndex + 1}:
                                            </label>
                                            <input
                                            type="text"
                                            id={`pictureUrl${index}-${pictureIndex}`}
                                            name={`pictureUrl${index}-${pictureIndex}`}
                                            value={picture.link}
                                            onChange={(event) =>
                                                handlePictureUrlChange(index, pictureIndex, event)
                                            }
                                            required
                                            />
                                        </div>
                                        ))}
                                            
                                    <button
                                        type="button"
                                        onClick={() => {
                                        const updatedOptions = [...options];
                                        updatedOptions[index].pictureUrl.push("");
                                        setOptions(updatedOptions);
                                        }}
                                    >
                                        Add Picture URL
                                    </button>
                                </div>
                            ))}
                            <button type="button" onClick={addOption}>
                                Add Option
                            </button>
                            </fieldset>

                            <fieldset>
                            <legend>Prices:</legend>
                            {prices.map((price, index) => (
                                <div className="price" key={index}>
                                <label htmlFor={`priceId${index + 1}`}>Price ID:</label>
                                <input
                                    type="number"
                                    id={`priceId${index + 1}`}
                                    name="priceId"
                                    value={price.priceId}
                                    onChange={(event) => handlePriceChange(index, event)}
                                    required
                                />

                                <label htmlFor={`size${index + 1}`}>Size:</label>
                                <input
                                    type="text"
                                    id={`size${index + 1}`}
                                    name="size"
                                    value={price.size}
                                    onChange={(event) => handlePriceChange(index, event)}
                                    required
                                />

                                <label htmlFor={`amount${index + 1}`}>Amount:</label>
                                <input
                                    type="number"
                                    id={`amount${index + 1}`}
                                    name="amount"
                                    value={price.amount}
                                    onChange={(event) => handlePriceChange(index, event)}
                                    required
                                />
                                </div>
                            ))}
                            <button type="button" onClick={addPrice}>
                                Add Price
                            </button>
                            </fieldset>

                            <label htmlFor="bannerPicUrl">Banner Picture URL:</label>
                            <input
                            type="text"
                            id="bannerPicUrl"
                            name="bannerPicUrl"
                            value={bannerPicUrl}
                            onChange={(event) => setBannerPicUrl(event.target.value)}
                            required
                            />

                            <button type="submit">Submit</button>
                        </form>
                    </section>
                </>
            }
        </>
    )
}