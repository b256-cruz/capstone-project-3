import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom"
import "../scss/userDashboard.scss"


export default function UserDashboard() {

    const [loading, setLoading] = useState(true)
    const [user, setUser] = useState([])
    const [cart, setCart] = useState([])
    const { id } =useParams()

    useEffect(() => {
        async function fetchData() {
            try {
                const userResponse = await fetch(`http://localhost:4000/users/retrieveUser/${id}`,{
                    method: "GET",
                    credentials: "include"
                })
                const userData = await userResponse.json();
                setUser(userData)
                setLoading(false);
            } catch (err) {
                console.log(err);
            }
        }
        fetchData();

        async function fetchCart() {
            try {
                const cartResponse = await fetch(`http://localhost:4000/users/cart/${id}`,{
                    method: "GET",
                    credentials: "include"
                })
                const cartData = await cartResponse.json()
                setCart(cartData)
                setLoading(false);
            } catch (err) {
                console.log(err);
            }
        }
        fetchCart()
    },[]);

    console.log(cart.userCart)
    const cartData = cart.userCart && cart.userCart.map((useCart) => {
        return(
            <div className="order_container">
                <img src={useCart.orderPic}></img>
                <div>
                    <h1>{useCart.orderName}</h1>
                    <p>Color: {useCart.orderColor}</p>
                    <p>Size: {useCart.orderSize}</p>
                    <p>Quantity: {useCart.orderQuantity}</p>
                    <p>Price: {useCart.orderPrice}</p>
                </div>
            </div>
        )
    })

    return(
        <section className="userDashboard_container">
            {loading ? (
                <p>Loading...</p>
            ) : (
                <>
                    <div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 256 256">
                                <path fill="currentColor" d="M128 24a104 104 0 1 0 104 104A104.11 104.11 0 0 0 128 24ZM74.08 197.5a64 64 0 0 1 107.84 0a87.83 87.83 0 0 1-107.84 0ZM96 120a32 32 0 1 1 32 32a32 32 0 0 1-32-32Zm97.76 66.41a79.66 79.66 0 0 0-36.06-28.75a48 48 0 1 0-59.4 0a79.66 79.66 0 0 0-36.06 28.75a88 88 0 1 1 131.52 0Z"/>
                            </svg>
                            <div>
                                <h1>{user.firstName} {user.lastName}</h1>
                                <h2>{user.email}</h2>
                                <h3>
                                    {
                                        (user.isAdmin === false)?
                                        <>
                                            User
                                        </>
                                        :
                                        <>
                                            Admin
                                        </>
                                    }
                                </h3>
                            </div>
                        </div>
                        <div>
                            <h1>Order History</h1>
                        </div>
                    </div>
                    <form className="cart_container"> 
                        <h1>Cart</h1>
                        {cartData}
                        <Link to={`/checkout/${id}`}>
                            <button type="submit">
                                CHECK OUT
                            </button>
                        </Link>
                    </form>
                </>
                )}
        </section>
    )
}