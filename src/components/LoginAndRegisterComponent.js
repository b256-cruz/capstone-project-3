import { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css';
import "../scss/LoginAndRegisterComponent.scss"
import "../scss/app.scss"

export default function LoginAndRegisterComponent() {

    const navigate = useNavigate()
    const [ registerState, setRegisterState ] = useState(false)

    const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState('');

    const [loginEmail, setloginEmail] = useState("")
    const [loginPassword, setloginPassword] = useState("")
    

    const registerUser = async (e) => {
        try{
            e.preventDefault()

            if(password1 !== password2){
                toast.error('Password does not match vefiry password', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    })
                return
            }

            const checkEmailResponse = await fetch("http://localhost:4000/users/checkEmail",{
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    email: email,
                })
            })
            const checkEmailData = await checkEmailResponse.json()

            if(checkEmailData === true){
                toast.error('Email had already been used', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    })
                setEmail("")
                return
            }

            const registerResponse = await fetch("http://localhost:4000/users/register", {
                method: "POST",
                headers: {
                    "content-type": "application/json"
                },
                body: JSON.stringify({
                    firstName: firstName,
				    lastName: lastName,
                    email: email,
				    password: password1
                })
            })

            const registerData = await registerResponse.json()

            if(registerData === true){
                toast.success(' User Successfully registered!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    });
                setEmail("")
                setFirstName("")
                setLastName("")
                setPassword1("")
                setPassword2("")
                setRegisterState(false)
                return
            }else{
                toast.error('Unsuccessfully register user', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    })
                return
            }
                
        }catch(err){
            console.log(err)
            toast.error('something really went wrong', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
                })
            return
        }
    }

    const loginUser = async (e) => {
        try{
            e.preventDefault()

            const loginResponse = await fetch("http://localhost:4000/users/login", {
                method: "POST",
                credentials: 'include',
                headers: {
                    "content-type": "application/json"
                },
                body: JSON.stringify({
                    email: loginEmail,
				    password: loginPassword
                })
            })

            const loginData = await loginResponse.json()

            if(loginData === true) {
                toast.success(' User Successfully login', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
                setTimeout(() => {
                    navigate("/AllProduct");
                    window.location.reload();
                }, 1000); 
                
                return;

            }else {
                toast.error(' User Doesnt Exist', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    })
                return 
            }

        }catch(err) {
            console.log(err)
            toast.error('something really went wrong', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
                })
            return
        }
    }

    return (
        <section className="LoginAndRegisterComponent_container">
            <div className="LoginAndRegisterComponent_leftside_container">
                {
                    (registerState === true) ?
                    <>
                        <img src="https://live.staticflickr.com/65535/52891618461_656097b45b_o.jpg"></img>
                    </>
                    :
                    <>
                        <img src='https://live.staticflickr.com/65535/52893002331_0cd291a5c1_o.jpg'></img>
                    </>
                }
            </div>
            <div className="LoginAndRegisterComponent_rightside_container">
                <div>
                    {
                        (registerState === true) ?
                        <>
                            <button onClick={() => setRegisterState(true)}>REGISTER</button>
                            <button className="not_active" onClick={() => setRegisterState(false)}>LOGIN</button>
                        </>
                        :
                        <>
                            <button className="not_active" onClick={() => setRegisterState(true)}>REGISTER</button>
                            <button onClick={() => setRegisterState(false)}>LOGIN</button>
                        </>
                    }
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" fill="none">
                        <g clip-path="url(#clip0_28_302)">
                            <path d="M46 70.8333V79.1667C46 84.1667 49.3333 87.5 54.3333 87.5H91.8333C96.8333 87.5 100.167 84.1667 100.167 79.1667V20.8333C100.167 15.8333 96.8333 12.5 91.8333 12.5H54.3333C49.3333 12.5 46 15.8333 46 20.8333V29.1667H54.3333V20.8333H91.8333V79.1667H54.3333V70.8333H46Z" fill="black"/>
                            <path d="M60.8333 27L55 33.6667L67.5 46.1667H25V54.5H67.5L55 67L60.8333 72.8333L83.3333 50.3333L60.8333 27Z" fill="black"/>
                        </g>
                        <defs>
                            <clipPath id="clip0_28_302">
                            <rect width="100" height="100" fill="white"/>
                            </clipPath>
                        </defs>
                    </svg>
                    <p>
                        {
                            (registerState === true) ? "Create an Account" : "Sign in to your account"
                        }                   
                    </p>
                </div>
                {
                    (registerState === true) ?
                    <>
                        <div className="LoginAndRegisterComponent_rightside_register_container">
                            <div>
                                <form onSubmit={registerUser}>
                                    <div>
                                        <div>
                                            <span>
                                                <label for="firstName">First name:</label>
                                                <input type="text" id="firstName" name="firstName" value ={firstName} onChange={e => setFirstName(e.target.value)} required />
                                            </span>
                                            <span>
                                                <label for="lastName">Last name:</label>
                                                <input type="text" id="lastName" name="lastName" value={lastName} onChange={e => setLastName(e.target.value)} required />
                                            </span>                        
                                        </div>
                                        <div>
                                            <span>
                                                <label for="email">Email:</label>
                                                <input type="email" id="email" name="email" value={email} onChange={e => setEmail(e.target.value)} required />
                                            </span>
                                        </div>
                                        <div>
                                            <span>
                                                <label for="password1">Password:</label>
                                                <input type="password" id="password1" name="password1" value={password1} onChange={e => setPassword1(e.target.value)} required />
                                            </span>
                                            <span>
                                                <label for="password2">Verify Password:</label>
                                                <input type="password" id="password2" name="password2" value={password2} onChange={e => setPassword2(e.target.value)} required />
                                            </span>
                                        </div>
                                    </div>
                                    <p>By creating an account, you agree to Aqua Vault's Privacy Policy and Terms & Conditions</p>
                                    <button type="submit">REGISTER</button>
                                </form>
                            </div>
                        </div>
                    </>
                    :
                    <>
                        <div className="LoginAndRegisterComponent_rightside_login_container">
                            <form onSubmit = {loginUser}>
                                <div>
                                    <span>
                                        <label for="loginEmail">Email:</label>
                                        <input type="text" id="loginEmail" name="loginEmail" value={loginEmail} onChange={e => setloginEmail(e.target.value)} required />
                                    </span>
                                    <span>
                                        <label for="loginPassword">Password:</label>
                                        <input type="password" id="loginPassword" name="loginPassword" value={loginPassword} onChange={e => setloginPassword(e.target.value)} required />
                                    </span>                        
                                </div>
                                <p>By creating an account, you agree to Aqua Vault's Privacy Policy and Terms & Conditions</p>
                                <button type="submit">LOGIN</button>
                            </form>
                        </div>
                    </>
                }
            <ToastContainer />
            </div>
        </section>
    )
}